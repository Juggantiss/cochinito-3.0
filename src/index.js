import React from "react";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";

import LoginScreen from "./screens/Login";
import MainScreen from "./screens/Main";
import DepositarScreen from "./screens/Depositar";
import RetirarScreen from "./screens/Retirar";
import ConsultarScreen from "./screens/Consultar";
import HomeCuentaScreen from "./screens/cuenta/HomeCuenta";
import AddCuentaScreen from "./screens/cuenta/AddCuenta";
import RegistroScreen from "./screens/Registro";
import BotoneraBaja from "../src/components/BottomBar";
import HuellaScreen from "./screens/Huella";
import SplashScreen from "./screens/SplashScreen";
import SignInScreen from "./screens/SignInScreen";
import SignUpScreen from "./screens/SignUpScreen";

const Stack = createStackNavigator();

export default () => (
  <NavigationContainer>
    <Stack.Navigator
      initialRouteName="SplashScreen"
      screenOptions={{ headerShown: false }}
      headerMode="none"
    >
      <Stack.Screen name="SplashScreen" component={SplashScreen} />
      <Stack.Screen name="SignInScreen" component={SignInScreen} />
      <Stack.Screen name="SignUpScreen" component={SignUpScreen} />
      <Stack.Screen name="LoginScreen" component={LoginScreen} />
      <Stack.Screen name="HuellaScreen" component={HuellaScreen} />
      <Stack.Screen name="MainScreen" component={MainScreen} />
      <Stack.Screen name="DepositarScreen" component={DepositarScreen} />
      <Stack.Screen name="RetirarScreen" component={RetirarScreen} />
      <Stack.Screen name="ConsultarScreen" component={ConsultarScreen} />
      <Stack.Screen name="HomeCuentaScreen" component={HomeCuentaScreen} />
      <Stack.Screen name="AddCuentaScreen" component={AddCuentaScreen} />
      <Stack.Screen name="RegistroScreen" component={RegistroScreen} />
      <Stack.Screen name="BotoneraScreen" component={BotoneraBaja} />
    </Stack.Navigator>
  </NavigationContainer>
);
