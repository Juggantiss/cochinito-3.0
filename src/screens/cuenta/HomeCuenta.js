import React, { useState, useEffect } from "react";
import {
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
  ScrollView,
  AsyncStorage,
  Linking,
} from "react-native";
import { ListItem, Overlay, Icon, Button } from "react-native-elements";
import Toast from "../../utils/mensajeEnVentana";
import PreLoader from "../../components/PreLoader";
import Input from "../../components/Input";

export default function HomeCuenta({ navigation }) {
  const [cuenta, setCuenta] = useState([]);
  const [id, setId] = useState(0);
  const [visible, setVisible] = useState(true);
  const [OverlayVis, setOverVisible] = useState(false);
  const [Vis, setVis] = useState(false);
  const [Vis2, setVis2] = useState(false);
  const [Vis3, setVis3] = useState(false);
  const [Vis4, setVis4] = useState(true);
  const [Vis5, setVis5] = useState(false);
  const [load, setLoad] = useState(false);
  const [clv, setClv] = useState("");
  const [admin, setAdmin] = useState("");
  const [usuarios, setUsuarios] = useState([]);
  const [esadmin, setEsadmin] = useState(false);
  const user = React.createRef();
  const [usuario, setUsuario] = useState("");
  const [errorU, setErrorU] = useState("");
  const [iduser, setIduser] = useState(0);
  const [datos, setDatos] = useState([]);

  async function traerCuentasUser() {
    const getToken = async () => {
      const token = await AsyncStorage.getItem("Token");
      if (token) {
        setId(token);
      }
      console.log("token " + token);
      const url =
        "https://api-cochinito.herokuapp.com/rtiene/one/cuenta/" + token;

      try {
        const response = await fetch(url);
        const data = await response.json();
        const account = data.Cuenta;
        account.shift();
        setCuenta(account);
        setVisible(false);
      } catch (error) {
        console.log(error);
        Toast("ERROR AL TRAER LAS CUENTAS");
      }
    };
    getToken();
  }

  useEffect(() => {
    traerCuentasUser();
  }, []);

  useEffect(() => {
    const unsubscribe = navigation.addListener("focus", async () => {
      traerCuentasUser();
    });
    return unsubscribe;
  }, [navigation]);

  function vistaOverlay() {
    return (
      <>
        <View style={{ flex: 5 }}>
          <Text style={styles.tittleOver}>Usuarios en la cuenta</Text>
          {usuarios.map((l, i) => (
            <ListItem
              key={i}
              leftAvatar={<Icon name="user" type="font-awesome" />}
              title={l.usuario}
              rightTitle={
                esadmin ? (
                  l.usuario == admin ? (
                    ""
                  ) : (
                    <Icon
                      name="delete"
                      color="#D50000"
                      onPress={() => {
                        setIduser(l.id_usuario);
                        setVis(true);
                        console.log(l.id_usuario);
                      }}
                    />
                  )
                ) : (
                  ""
                )
              }
              subtitle={l.usuario == admin ? "Creador" : "Colaborador"}
              bottomDivider
              onPress={() => {
                if (esadmin) {
                  buscarDatos(l.id_usuario);
                  console.log("se presiono");
                }
              }}
            />
          ))}
        </View>
        {mostrarBotones()}
      </>
    );
  }

  function vistaDetallesUsuarios() {
    if (Vis5) {
      return (
        <ScrollView>
          <View>
            <Text style={styles.text}>Datos del usuario</Text>
            <Text style={styles.textOverlayTittle}>Usuario:</Text>
            <Text style={styles.textOverlayChild}>{datos.usuario}</Text>
            <Text style={styles.textOverlayTittle}>Nombre:</Text>
            <Text style={styles.textOverlayChild}>
              {datos.persona.nombre +
                " " +
                datos.persona.app +
                " " +
                datos.persona.apm +
                " "}
            </Text>
            <Text style={styles.textOverlayTittle}>Telefono:</Text>
            <Text
              style={[
                styles.textOverlayChild,
                { color: "#039be5", textDecorationLine: "underline" },
              ]}
              onPress={() => {
                Linking.openURL("tel:" + datos.persona.telefono);
              }}
            >
              {datos.persona.telefono}
            </Text>
            <Text style={styles.textOverlayTittle}>Edad:</Text>
            <Text style={styles.textOverlayChild}>{datos.persona.edad}</Text>
            <Text style={styles.textOverlayTittle}>Ciudad:</Text>
            <Text style={styles.textOverlayChild}>
              {datos.persona.ciudad + ", " + datos.persona.estado}
            </Text>
          </View>
        </ScrollView>
      );
    } else {
      return (
        <View>
          <Text></Text>
        </View>
      );
    }
  }

  async function buscarDatos(identificador) {
    const url =
      "https://api-cochinito.herokuapp.com/usuario/onep/" + identificador;
    try {
      const response = await fetch(url);
      const data = await response.json();
      setDatos(data);
      setVis5(true);
    } catch (error) {
      console.log(error);
      Toast("ERROR AL TRAER LOS DATOS DEL USUARIO");
    }
  }

  function vistaOverlayUsuario() {
    return (
      <View>
        <Text style={styles.tittleOver2}>Agregar Usuario</Text>
        <View
          style={{
            marginTop: 20,
            width: "100%",
            alignContent: "center",
            justifyContent: "center",
          }}
        >
          <Input
            ref={user}
            placeholder="Usuario"
            label="Ingresa el nombre de usuario"
            onChangeText={(value) => {
              setUsuario(value);
              setErrorU("");
            }}
            errorMessage={errorU}
          />
          <Button
            title="Agregar"
            loading={Vis3}
            icon={<Icon name="done" type="material" size={15} color="white" />}
            buttonStyle={{
              backgroundColor: "#00C853",
              marginTop: 30,
              marginLeft: "20%",
              width: 170,
              shadowColor: "#000",
              shadowOffset: {
                width: 0,
                height: 4,
              },
              shadowOpacity: 0.32,
              shadowRadius: 5.46,
              elevation: 9,
            }}
            onPress={() => {
              validarCampo();
            }}
          />
        </View>
      </View>
    );
  }

  function validarCampo() {
    if (usuario === "") {
      setErrorU("EL USUARIO ES NECESARIO");
      console.log(clv);
      user.current.shake();
      user.current.focus();
    } else {
      agregarUsuario();
      setVis3(true);
    }
  }

  function agregarUsuario() {
    const url =
      "https://api-cochinito.herokuapp.com/rtiene/create/" +
      usuario +
      "/" +
      clv;
    fetch(url, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
    })
      .then((response) => response.json())
      .then((json) => {
        console.log(json);
        Toast(json);
      })
      .catch((err) => {
        console.log(err);
        Toast("Error al añadir al usuario a la cuenta");
      })
      .finally(() => {
        setVis3(false);
        console.log("se termino el fetch");
      });
    user.current.clear();
    setUsuario("");
  }

  function mostrarBotones() {
    if (Vis4) {
      return (
        <View style={{ flex: 0.65, alignItems: "center" }}>
          <PreLoader isVisible={true} color={"#039be5"} />
        </View>
      );
    } else {
      if (esadmin) {
        return (
          <View style={{ flex: 0.65, alignItems: "center" }}>
            <Button
              title="Agregar usuario"
              buttonStyle={{
                paddingRight: "16%",
                width: "90%",
                backgroundColor: "#039be5",
                shadowColor: "#000",
                shadowOffset: {
                  width: 0,
                  height: 4,
                },
                shadowOpacity: 0.32,
                shadowRadius: 5.46,
                elevation: 9,
              }}
              onPress={() => setVis2(true)}
            />
          </View>
        );
      } else {
        return (
          <View style={{ flex: 0.65, alignItems: "center" }}>
            <Button
              title="Abandonar cuenta"
              buttonStyle={{
                paddingRight: "15%",
                width: "90%",
                backgroundColor: "#ff2121",
                shadowColor: "#000",
                shadowOffset: {
                  width: 0,
                  height: 4,
                },
                shadowOpacity: 0.32,
                shadowRadius: 5.46,
                elevation: 9,
              }}
              onPress={() => setVis(true)}
            />
          </View>
        );
      }
    }
  }

  function buscarAdmin(clave) {
    const url =
      "https://api-cochinito.herokuapp.com/rtiene/one/usuario/" + clave;
    fetch(url)
      .then((response) => response.json())
      .then((usuarios) => {
        setAdmin(usuarios[0].usuario);
        setUsuarios(usuarios);
        console.log(usuarios[0].usuario);
        const url2 = "https://api-cochinito.herokuapp.com/usuario/one/" + id;
        fetch(url2)
          .then((response) => response.json())
          .then((u) => {
            if (u.usuario == usuarios[0].usuario) {
              setEsadmin(true);
              setVis4(false);
            } else {
              setEsadmin(false);
            }
          })
          .catch((err) => {
            console.log(err);
            Toast("ERROR AL TRAER EL USUARIO");
          })
          .finally(() => console.log("si trajo al usuario"));
      })
      .catch((err) => {
        console.log(err);
        Toast("ERROR AL TRAER LOS USUARIOS");
      })
      .finally(() => console.log("si trajo usuarios"));
  }

  function eliminarUsuario() {
    let url = "";
    if (esadmin) {
      url =
        "https://api-cochinito.herokuapp.com/rtiene/delete/" +
        iduser +
        "/" +
        clv;
    } else {
      url =
        "https://api-cochinito.herokuapp.com/rtiene/delete/" + id + "/" + clv;
    }
    fetch(url, {
      method: "DELETE",
      headers: {
        "Content-Type": "application/json",
      },
    })
      .then((response) => response.json())
      .then((elim) => {
        console.log(elim);
        if (esadmin) {
          Toast("Usuario eliminado");
          setOverVisible(false);
        } else {
          Toast(elim);
          setOverVisible(false);
          traerCuentasUser();
        }
      })
      .catch((err) => {
        console.log(err);
        Toast("ERROR AL ABANDONAR LA CUENTA");
      })
      .finally(() => {
        setLoad(false);
        setVis(false);
      });
  }

  function vistaEliminar() {
    return (
      <View>
        <Text style={styles.tittleOver2}>¿Estás seguro?</Text>
        <View
          style={{
            flexDirection: "row",
            marginTop: 30,
            width: "100%",
            alignContent: "space-between",
            justifyContent: "center",
          }}
        >
          <Button
            title="Aceptar"
            loading={load}
            icon={<Icon name="done" type="material" size={15} color="white" />}
            buttonStyle={{
              backgroundColor: "#00C853",
              marginRight: 30,
              width: 100,
              shadowColor: "#000",
              shadowOffset: {
                width: 0,
                height: 4,
              },
              shadowOpacity: 0.32,
              shadowRadius: 5.46,
              elevation: 9,
            }}
            onPress={() => {
              eliminarUsuario();
              setLoad(true);
            }}
          />
          <Button
            title="Cancelar"
            icon={
              <Icon name="warning" type="material" size={15} color="white" />
            }
            buttonStyle={{
              width: 100,
              backgroundColor: "#ff2121",
              shadowColor: "#000",
              shadowOffset: {
                width: 0,
                height: 4,
              },
              shadowOpacity: 0.32,
              shadowRadius: 5.46,
              elevation: 9,
            }}
            onPress={() => {
              setVis(false);
            }}
          />
        </View>
      </View>
    );
  }

  function mostrarCuentas() {
    if (cuenta.length < 1) {
      if (visible) {
        return <PreLoader isVisible={true} color={"#039be5"} />;
      } else {
        return (
          <>
            <View style={styles.container}>
              <Text style={styles.texttittle}>
                No tienes cuentas compartidas
              </Text>
              <Text style={styles.texttittle}>crea una acá</Text>
              <TouchableOpacity
                style={styles.button}
                onPress={() => {
                  console.log("se presiono");
                  navigation.navigate("AddCuentaScreen");
                }}
              >
                <Text style={styles.textbot}>Crear</Text>
              </TouchableOpacity>
            </View>
          </>
        );
      }
    } else {
      return (
        <>
          <ScrollView>
            <Text style={styles.tittle}>Cuentas compartidas</Text>
            {cuenta.map((item, i) => (
              <ListItem
                key={i}
                title={item.descripcion}
                subtitle={item.fecha_creacion}
                rightTitle={"Saldo"}
                rightSubtitle={"$ " + item.saldo}
                bottomDivider
                chevron
                onPress={() => {
                  setOverVisible(true);
                  setClv(item.clv_cuenta);
                  buscarAdmin(item.clv_cuenta);
                }}
              />
            ))}
          </ScrollView>
          <View style={{ alignItems: "flex-end", marginBottom: "2%" }}>
            <Icon
              raised
              reverse
              name="plus"
              type="font-awesome"
              color="#039be5"
              onPress={() => navigation.navigate("AddCuentaScreen")}
            />
          </View>
        </>
      );
    }
  }

  return (
    <View style={styles.container2}>
      <Overlay
        isVisible={OverlayVis}
        onBackdropPress={() => {
          setOverVisible(false);
        }}
      >
        {vistaOverlay()}
      </Overlay>
      <Overlay
        isVisible={Vis}
        height={130}
        width={300}
        borderRadius={15}
        overlayBackgroundColor={"#FFFFFF"}
        onBackdropPress={() => {
          setVis(false);
        }}
      >
        {vistaEliminar()}
      </Overlay>
      <Overlay
        isVisible={Vis2}
        height={300}
        width={300}
        borderRadius={15}
        overlayBackgroundColor={"#FFFFFF"}
        onBackdropPress={() => {
          setErrorU("");
          setUsuario("");
          user.current.clear();
          setVis2(false);
          setOverVisible(false);
        }}
      >
        {vistaOverlayUsuario()}
      </Overlay>
      <Overlay
        isVisible={Vis5}
        height={300}
        width={250}
        overlayBackgroundColor={"#FFFFFF"}
        onBackdropPress={() => {
          setVis5(false);
        }}
      >
        {vistaDetallesUsuarios()}
      </Overlay>
      {mostrarCuentas()}
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    flexDirection: "column",
    marginTop: 25,
    justifyContent: "center",
    alignItems: "center",
  },
  container2: {
    flex: 1,
    backgroundColor: "#fff",
    flexDirection: "column",
    marginTop: 25,
  },
  texttittle: {
    color: "#039be5",
    fontSize: 20,
    textAlign: "center",
    fontWeight: "bold",
  },
  tittle: {
    color: "#039be5",
    fontSize: 30,
    textAlign: "center",
    fontWeight: "bold",
  },
  tittleOver: {
    color: "#039be5",
    fontSize: 25,
    textAlign: "center",
    fontWeight: "bold",
  },
  tittleOver2: {
    color: "#039be5",
    fontSize: 25,
    textAlign: "center",
    fontWeight: "bold",
  },
  button: {
    borderColor: "red",
    backgroundColor: "#039be5",
    paddingVertical: 10,
    borderRadius: 25,
    height: 45,
    width: "40%",
    marginLeft: "5%",
    marginTop: "5%",
    marginRight: 10,
  },
  button2: {
    borderColor: "red",
    backgroundColor: "#039be5",
    paddingVertical: 10,
    borderRadius: 25,
    height: 45,
    width: "40%",
    marginLeft: "5%",
    marginTop: "5%",
    marginRight: 10,
  },
  textbot: {
    color: "white",
    fontSize: 20,
    textAlign: "center",
    fontWeight: "bold",
  },
  textOverlayTittle: {
    marginTop: 20,
    color: "#000",
    fontSize: 20,
    fontWeight: "bold",
  },
  textOverlayChild: {
    color: "#000",
    fontSize: 15,
  },
  text: {
    color: "#039be5",
    fontSize: 20,
    textAlign: "left",
    textTransform: "uppercase",
    fontWeight: "bold",
  },
});
