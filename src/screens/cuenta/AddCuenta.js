import React, { useState, useEffect } from "react";
import {
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
  AsyncStorage,
} from "react-native";
import Input from "../../components/Input";
import Boton from "../../components/Boton";
import PreLoader from "../../components/PreLoader";
import Toast from "../../utils/mensajeEnVentana";
import { styles } from "../../components/Input/styles";
import uuid from "react-native-uuid";

export default function AddCuenta({ navigation }) {
  const [error, setError] = useState("");
  const [errorU, setErrorU] = useState("");
  const [nombre, setNombre] = useState("");
  const [usuario, setUsuario] = useState("");
  const [habilitar, setHabilitar] = useState(true);
  const [habilitarb, setHabilitarb] = useState(false);
  const [listo, setListo] = useState(false);
  const [visible, setVisible] = useState(false);
  const [visible1, setVisible1] = useState(false);
  const [id, setId] = useState(0);
  const [clv, setClv] = useState("");
  const nom = React.createRef();
  const user = React.createRef();

  function validarCampo(opcion) {
    if (opcion == 1) {
      if (nombre === "") {
        setError("EL NOMBRE ES NECESARIO");
        nom.current.shake();
        nom.current.focus();
      } else {
        crearCuenta();
        setVisible1(true);
      }
    } else {
      if (usuario === "") {
        setErrorU("EL USUARIO ES NECESARIO");
        console.log(clv);
        user.current.shake();
        user.current.focus();
      } else {
        agregarUsuario();
        setVisible(true);
      }
    }
  }

  function agregarUsuario() {
    const url =
      "https://api-cochinito.herokuapp.com/rtiene/create/" +
      usuario +
      "/" +
      clv;
    fetch(url, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
    })
      .then((response) => response.json())
      .then((json) => {
        console.log(json);
        Toast(json);
      })
      .catch((err) => {
        console.log(err);
        Toast("Error al añadir al usuario a la cuenta");
      })
      .finally(() => {
        setVisible(false);
        console.log("se termino el fetch");
      });
    user.current.clear();
    setUsuario("");
  }

  useEffect(() => {
    const getToken = async () => {
      const token = await AsyncStorage.getItem("Token");
      if (token) {
        setId(token);
      }
    };
    getToken();
  }, []);

  function crearCuenta() {
    const gen = uuid.v4();
    setClv(gen);
    const url =
      "https://api-cochinito.herokuapp.com/cuenta/create/" +
      id +
      "/" +
      nombre +
      "/" +
      gen;
    fetch(url, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
    })
      .then((response) => response.json())
      .then((json) => {
        console.log(json);
        Toast(json);
      })
      .catch((err) => {
        console.log(err);
        Toast("Error al crear la cuenta compartida");
      })
      .finally(() => {
        setVisible1(false);
        console.log("se termino el fetch");
        setHabilitar(false);
        setHabilitarb(true);
        setListo(true);
      });
    nom.current.clear();
    setNombre("");
  }

  function mostrarLoader(op) {
    if (op == 1) {
      if (visible) {
        return <PreLoader isVisible={true} color={"#fff"} />;
      } else {
        return <Text style={styles1.textbot}>Agregar</Text>;
      }
    } else {
      if (visible1) {
        return <PreLoader isVisible={true} color={"#fff"} />;
      } else {
        return <Text style={styles1.textbot}>Crear</Text>;
      }
    }
  }

  function mostrarAgregar() {
    if (listo) {
      return (
        <>
          <Text style={styles1.texttittle2}>Agregar usuario a la cuenta</Text>
          <Input
            ref={user}
            placeholder="Usuario"
            label="Ingresa el nombre de usuario"
            onChangeText={(value) => {
              setUsuario(value);
              setErrorU("");
            }}
            errorMessage={errorU}
          />
          <TouchableOpacity
            style={styles1.button}
            onPress={() => {
              console.log("se presiono");
              validarCampo(2);
            }}
          >
            {mostrarLoader(1)}
          </TouchableOpacity>

          <Boton
            titulo="Regresar"
            margenSuperior={30}
            ancho={"40%"}
            color={"red"}
            alPresionar={() => {
              setHabilitarb(false);
              setHabilitar(true);
              setListo(false);
              navigation.navigate("BotoneraScreen");
            }}
          />
        </>
      );
    }
  }

  return (
    <View style={styles1.container}>
      <Text style={styles1.texttittle}>Crear cuenta compartida</Text>
      <Input
        ref={nom}
        placeholder="Nombre"
        label="Ingresa el nombre"
        editable={habilitar}
        onChangeText={(value) => {
          setNombre(value);
          setError("");
        }}
        errorMessage={error}
      />
      <TouchableOpacity
        style={habilitarb ? styles1.button2 : styles1.button}
        disabled={habilitarb}
        onPress={() => {
          console.log("se presiono");
          validarCampo(1);
        }}
      >
        {mostrarLoader(2)}
      </TouchableOpacity>
      {mostrarAgregar()}
    </View>
  );
}

const styles1 = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    flexDirection: "column",
    marginTop: 25,
    justifyContent: "flex-start",
    alignItems: "center",
  },
  texttittle: {
    color: "#039be5",
    fontSize: 25,
    textAlign: "center",
    fontWeight: "bold",
  },
  texttittle2: {
    color: "#039be5",
    fontSize: 20,
    marginTop: "10%",
    textAlign: "center",
    fontWeight: "bold",
  },
  button: {
    borderColor: "red",
    backgroundColor: "#039be5",
    paddingVertical: 10,
    borderRadius: 25,
    height: 45,
    width: "40%",
    marginLeft: "5%",
    marginTop: "5%",
    marginRight: 10,
  },
  button2: {
    borderColor: "red",
    backgroundColor: "#cfcfcf",
    paddingVertical: 10,
    borderRadius: 25,
    height: 45,
    width: "40%",
    marginLeft: "5%",
    marginTop: "5%",
    marginRight: 10,
  },
  textbot: {
    color: "white",
    fontSize: 20,
    textAlign: "center",
    fontWeight: "bold",
  },
});
