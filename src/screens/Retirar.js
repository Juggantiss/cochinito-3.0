import React, { useState, useEffect } from "react";
import {
  StyleSheet,
  View,
  Text,
  Picker,
  ScrollView,
  AsyncStorage,
  TouchableOpacity,
} from "react-native";
import Input from "../components/Input";
import PreLoader from "../components/PreLoader";
import Toast from "../utils/mensajeEnVentana";

export default function Retirar({ navigation }) {
  const [selectedValue, setSelectedValue] = useState("Selecciona una cuenta");
  const [cuenta, setCuenta] = useState([]);
  const [cantidad, setCantidad] = useState("");
  const [motivo, setMotivo] = useState("");
  const [error, setError] = useState("");
  const [errorM, setErrorM] = useState("");
  const [id, setId] = useState(0);
  const [loader, setLoader] = useState(false);
  const input = React.createRef();
  const input2 = React.createRef();

  function validarCampos() {
    if (cantidad === "" || cantidad === "0") {
      setError("DEBES INGRESAR UNA CANTIDAD");
      input.current.shake();
      input.current.focus();
      console.log("error");
    } else if (motivo === "") {
      setErrorM("DEBES INGRESAR UN MOTIVO");
      input2.current.shake();
      input2.current.focus();
      console.log("error");
    } else {
      retirarSaldo();
      setLoader(true);
      input.current.clear();
      input2.current.clear();
      setCantidad("");
      setMotivo("");
    }
  }

  function retirarSaldo() {
    const cant = parseFloat(cantidad);
    const url =
      "https://api-cochinito.herokuapp.com/movimiento/create/" +
      id +
      "/2/" +
      selectedValue;
    fetch(url, {
      method: "POST",
      body: JSON.stringify({
        cantidad: cant,
        motivo,
      }),
      headers: {
        "Content-Type": "application/json",
      },
    })
      .then((response) => response.json())
      .then((json) => {
        console.log(json.resultado);
        Toast(json.resultado);
      })
      .catch((err) => {
        console.log(err);
        Toast("ERROR AL HACER EL RETIRO");
        input.current.shake();
        input.current.clear();
        input2.current.shake();
        input2.current.clear();
      })
      .finally(() => setLoader(false));
  }

  async function traerCuentasUser() {
    const getToken = async () => {
      const token = await AsyncStorage.getItem("Token");
      if (token) {
        setId(token);
      }
      console.log("token " + token);
      try {
        const url =
          "https://api-cochinito.herokuapp.com/rtiene/one/cuenta/" + token;
        const response = await fetch(url);
        const data = await response.json();
        setCuenta(data.Cuenta);
      } catch (error) {
        console.log(error);
        Toast("ERROR AL TRAER LAS CUENTAS");
      }
    };
    getToken();
  }

  useEffect(() => {
    traerCuentasUser();
  }, []);

  useEffect(() => {
    const unsubscribe = navigation.addListener("focus", async () => {
      traerCuentasUser();
    });
    return unsubscribe;
  }, [navigation]);

  function mostrarCuenta() {
    return cuenta.map((item, i) => (
      <Picker.Item
        label={item.descripcion}
        value={item.clv_cuenta}
        key={item.clv_cuenta}
      />
    ));
  }

  function mostrarLoader() {
    if (loader) {
      return <PreLoader isVisible={true} color={"#fff"} />;
    } else {
      return <Text style={styles.textbot}>Retirar</Text>;
    }
  }

  return (
    <ScrollView style={{ backgroundColor: "#fff" }}>
      <View style={styles.container}>
        <View style={styles.titulo}>
          <Text style={styles.text}>Retiro</Text>
        </View>
        <View style={styles.superior}>
          <Text style={styles.text}>Cuenta</Text>
          <Picker
            selectedValue={selectedValue}
            style={{ height: 50, width: 210, marginTop: 40 }}
            onValueChange={(itemValue, itemIndex) =>
              setSelectedValue(itemValue)
            }
          >
            {mostrarCuenta()}
          </Picker>
          <Input
            ref={input}
            inputContainerStyle={styles.contorno}
            placeholder="Cantidad"
            label="Ingresa la cantidad"
            onChangeText={(value) => {
              setCantidad(value);
              setError("");
            }}
            keyboardType="decimal-pad"
            errorMessage={error}
          />
          <Input
            ref={input2}
            placeholder="Motivo"
            label="Ingresa el motivo"
            onChangeText={(value) => {
              setMotivo(value);
              setErrorM("");
            }}
            keyboardType="default"
            multiline
            errorMessage={errorM}
          />
        </View>
        <View style={styles.botones}>
          <TouchableOpacity
            style={styles.button}
            onPress={() => {
              validarCampos();
            }}
          >
            {mostrarLoader()}
          </TouchableOpacity>
        </View>
      </View>
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    flexDirection: "column",
    marginTop: 30,
    justifyContent: "flex-start",
  },
  titulo: {
    flex: 0.5,
    backgroundColor: "#fff",
    marginLeft: 10,
  },
  superior: {
    flex: 2.75,
    backgroundColor: "#fff",
    alignItems: "center",
    marginTop: 0,
  },
  botones: {
    flex: 2,
    backgroundColor: "#fff",
    alignItems: "center",
    flexDirection: "column",
    justifyContent: "flex-start",
    marginTop: "10%",
  },
  text: {
    color: "#039be5",
    fontSize: 20,
    textAlign: "left",
    textTransform: "uppercase",
    fontWeight: "bold",
  },
  button: {
    borderColor: "red",
    backgroundColor: "#039be5",
    paddingVertical: 10,
    borderRadius: 25,
    height: 45,
    width: "50%",
    marginTop: 15,
  },
  textbot: {
    color: "white",
    fontSize: 20,
    textAlign: "center",
    fontWeight: "bold",
  },
  contorno: {
    width: "40%",
    borderWidth: 2,
    borderColor: "transparent",
    borderBottomColor: "#039be5",
  },
});
