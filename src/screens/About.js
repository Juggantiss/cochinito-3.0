import React from "react";
import {
  StyleSheet,
  View,
  Text,
  ScrollView,
  TouchableOpacity,
  Linking,
} from "react-native";
import { Avatar } from "react-native-elements";
import { AntDesign, Zocial } from "@expo/vector-icons";

export default function About({ navigation }) {
  return (
    <ScrollView
      style={{ height: "100%", width: "100%", backgroundColor: "#fff" }}
    >
      <View style={styles.container}>
        <Text style={styles.text}>Cochinito</Text>
        <Text style={styles.text2}>versión: 1.0</Text>
        <View style={{ flexDirection: "row", marginBottom: 50 }}>
          <Avatar
            rounded
            overlayContainerStyle={{ backgroundColor: "white", padding: 10 }}
            size="xlarge"
            source={require("../../assets/images/logo_cinere.png")}
          />
          <Avatar
            rounded
            overlayContainerStyle={{ backgroundColor: "white", padding: 10 }}
            size="xlarge"
            source={require("../../assets/images/logo_cara_nombre.png")}
          />
        </View>
        <TouchableOpacity
          style={[styles.button, { backgroundColor: "#ff0000" }]}
          onPress={() =>
            Linking.openURL(
              "https://www.youtube.com/channel/UCVhYc8PzUleXWBM4G-AKvrA/featured?view_as=subscriber"
            )
          }
        >
          <AntDesign name="youtube" size={24} color="white" />
          <Text style={styles.textbot}>Cinere</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={[styles.button, { backgroundColor: "#78909C" }]}
          onPress={() =>
            Linking.openURL(
              "mailto:cinerepc@gmail.com?subject=Contacto cochinito"
            )
          }
        >
          <Zocial name="email" size={24} color="white" />
          <Text style={styles.textbot}>Cinere</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={[styles.button, { backgroundColor: "#F50057" }]}
          onPress={() =>
            Linking.openURL("https://www.instagram.com/juggantiss/?hl=es-la")
          }
        >
          <AntDesign name="instagram" size={24} color="white" />
          <Text style={styles.textbot}>Juggantiss</Text>
        </TouchableOpacity>
      </View>
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "flex-start",
  },
  text: {
    marginTop: 35,
    color: "#039be5",
    fontSize: 30,
    textAlign: "left",
    textTransform: "uppercase",
    fontWeight: "bold",
  },
  text2: {
    color: "#039be5",
    fontSize: 20,
    marginBottom: 40,
    textAlign: "left",
  },
  text3: {
    color: "#039be5",
    fontSize: 20,
    textAlign: "left",
  },
  button: {
    borderColor: "red",
    borderRadius: 25,
    height: 50,
    width: "60%",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    marginBottom: 15,
  },
  textbot: {
    color: "white",
    fontSize: 15,
    textAlign: "center",
    fontWeight: "bold",
    marginLeft: "5%",
  },
});
