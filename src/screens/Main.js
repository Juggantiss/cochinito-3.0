import React from "react";
import {
  ImageBackground,
  StyleSheet,
  View,
  Dimensions,
  ScrollView,
} from "react-native";
import Boton from "../components/Boton";

const image = require("../../assets/images/main.png");
const { height, width } = Dimensions.get("window");

export default function Main({ navigation }) {
  return (
    <ScrollView style={{ backgroundColor: "#fff" }}>
      <View style={styles.container}>
        <ImageBackground source={image} style={styles.image}>
          <Boton
            titulo="Depositar"
            alPresionar={() => {
              //alert("Depositar");
              navigation.navigate("DepositarScreen");
            }}
          />
          <Boton
            titulo="Retirar"
            alPresionar={() => {
              //alert("Retirar");
              navigation.navigate("RetirarScreen");
            }}
            margenSuperior={65}
          />
          <Boton
            titulo="Consultar"
            alPresionar={() => {
              //alert("Consultar");
              navigation.navigate("ConsultarScreen");
            }}
            margenSuperior={65}
          />
          <Boton
            titulo="Cuentas"
            alPresionar={() => {
              //alert("Cuentas");
              navigation.navigate("HomeCuentaScreen");
            }}
            margenSuperior={65}
          />
        </ImageBackground>
      </View>
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
  },
  image: {
    flex: 1,
    width: width,
    height: height,
    justifyContent: "center",
    alignItems: "center",
  },
});
