import React from "react";
import {
  View,
  Text,
  TouchableOpacity,
  Platform,
  StatusBar,
  TextInput,
  StyleSheet,
  ScrollView,
} from "react-native";
import { LinearGradient } from "expo-linear-gradient";
import FontAwesome from "react-native-vector-icons/FontAwesome";
import * as Animatable from "react-native-animatable";
import Feather from "react-native-vector-icons/Feather";
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";
import Toast from "../utils/mensajeEnVentana";

export default function SignUpScreen({ navigation }) {
  const [data, setData] = React.useState({
    // Data Entry
    user: "",
    email: "",
    password: "",
    confirm_password: "",
    // Validations for Data Entry
    isValidUser: true,
    isValidEmail: true,
    isValidPassword: true,
    passwordCoincide: true,
    // Data Personal
    name: "",
    lastname: "",
    lastnamematter: "",
    telephone: 0,
    year: 0,
    // Validations for Data Personal
    isValidName: true,
    isValidApp: true,
    isValidApm: true,
    isValidYear: true,
    isValidPhone: true,
    // Icons Data Entry
    check_textInputUserChange: false,
    check_textInputEmailChange: false,
    secureTextEntry: true,
    confirm_secureTextEntry: true,
    // Icons Data Personal
    check_InputNameChange: false,
    check_InputAppChange: false,
    check_InputApmChange: false,
    check_InputYearChange: false,
    check_InputPhoneChange: false,
  });

  // States for show the views in the View Footer
  const [view, setView] = React.useState({
    showViewDataEntry: true,
    showViewDataPersonal: false,
    showViewDataAddress: false,
  });

  // Methods for Catch value on input and change the state for you icon
  const textInputUserChange = (value) => {
    if (value.trim().length >= 4) {
      setData({
        ...data,
        user: value,
        check_textInputUserChange: true,
        isValidUser: true,
      });
    } else {
      setData({
        ...data,
        user: value,
        check_textInputUserChange: false,
        isValidUser: false,
      });
    }
  };

  const textInputEmailChange = (value) => {
    if (
      value
        .trim()
        .match(
          "^[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?$"
        )
    ) {
      setData({
        ...data,
        email: value,
        check_textInputEmailChange: true,
        isValidEmail: true,
      });
    } else {
      setData({
        ...data,
        email: value,
        check_textInputEmailChange: false,
        isValidEmail: false,
      });
    }
  };

  const handlePasswordChange = (value) => {
    if (value.trim().length >= 8) {
      setData({
        ...data,
        password: value,
        isValidPassword: true,
      });
    } else {
      setData({
        ...data,
        password: value,
        isValidPassword: false,
      });
    }
  };

  const handleConfirmPasswordChange = (value) => {
    if (value.trim() === data.password) {
      setData({
        ...data,
        confirm_password: value,
        passwordCoincide: true,
      });
    } else {
      setData({
        ...data,
        confirm_password: value,
        passwordCoincide: false,
      });
    }
  };

  const textInputNameChange = (value) => {
    if (value.trim().length >= 3) {
      setData({
        ...data,
        name: value,
        check_InputNameChange: true,
        isValidName: true,
      });
    } else {
      setData({
        ...data,
        name: value,
        check_InputNameChange: false,
        isValidName: false,
      });
    }
  };

  const textInputAppChange = (value) => {
    if (value.trim().length >= 3) {
      setData({
        ...data,
        lastname: value,
        check_InputAppChange: true,
        isValidApp: true,
      });
    } else {
      setData({
        ...data,
        lastname: value,
        check_InputAppChange: false,
        isValidApp: false,
      });
    }
  };

  const textInputApmChange = (value) => {
    if (value.trim().length >= 3) {
      setData({
        ...data,
        lastnamematter: value,
        check_InputApmChange: true,
        isValidApm: true,
      });
    } else {
      setData({
        ...data,
        lastnamematter: value,
        check_InputApmChange: false,
        isValidApm: false,
      });
    }
  };

  const textInputYearChange = (value) => {
    if (value.trim().length > 0 && value >= 18 && value <= 120) {
      setData({
        ...data,
        year: value,
        check_InputYearChange: true,
        isValidYear: true,
      });
    } else {
      setData({
        ...data,
        year: value,
        check_InputYearChange: false,
        isValidYear: false,
      });
    }
  };

  const textInputPhoneChange = (value) => {
    if (value.trim().length == 10) {
      setData({
        ...data,
        telephone: value,
        check_InputPhoneChange: true,
        isValidPhone: true,
      });
    } else {
      setData({
        ...data,
        telephone: value,
        check_InputPhoneChange: false,
        isValidPhone: false,
      });
    }
  };

  // Update state of views for show in View Footer, receiving the name of view of want show
  const updateViews = (v) => {
    switch (v) {
      case "entry":
        setView({
          ...view,
          showViewDataEntry: true,
          showViewDataPersonal: false,
          showViewDataAddress: false,
        });
        break;
      case "personal":
        setView({
          ...view,
          showViewDataEntry: false,
          showViewDataPersonal: true,
          showViewDataAddress: false,
        });
        break;
      case "address":
        setView({
          ...view,
          showViewDataEntry: false,
          showViewDataPersonal: false,
          showViewDataAddress: true,
        });
        break;

      default:
        null;
        break;
    }
  };

  // Validation for inputs of Data Entrys, and change state to View Data Personal
  const check_InputsEntry = () => {
    if (data.length !== 0) {
      if (data.user != "") {
        if (data.email != "") {
          if (data.password != "") {
            if (data.confirm_password != "") {
              updateViews("personal");
            } else {
              setData({
                ...data,
                passwordCoincide: false,
              });
            }
          } else {
            setData({
              ...data,
              isValidPassword: false,
            });
          }
        } else {
          setData({
            ...data,
            isValidEmail: false,
          });
        }
      } else {
        setData({
          ...data,
          isValidUser: false,
        });
      }
    } else {
      Toast("Los campos no pueden estar vacios");
    }
  };

  // Validation for inputs of Data Personal, and change state for View Data Address
  const check_InputsPersonal = () => {
    if (data.length !== 0) {
      if (data.name != "") {
        if (data.app != "") {
          if (data.apm != "") {
            if (
              data.year != "" &&
              data.year !== 0 &&
              data.year >= 18 &&
              data.year <= 120
            ) {
              if (
                data.telephone != "" &&
                data.telephone !== 0 &&
                data.telephone.length == 10
              ) {
                updateViews("address");
              } else {
                setData({
                  ...data,
                  isValidPhone: false,
                });
              }
            } else {
              setData({
                ...data,
                isValidYear: false,
              });
            }
          } else {
            setData({
              ...data,
              isValidApm: false,
            });
          }
        } else {
          setData({
            ...data,
            isValidApp: false,
          });
        }
      } else {
        setData({
          ...data,
          isValidName: false,
        });
      }
    } else {
      Toast("Los campos no pueden estar vacios");
    }
  };

  // Validation for inputs of Data Address, and change state for next step, register the values
  const check_InputsAddress = () => {
    if (data.length !== 0) {
      if (data.user != "") {
        if (data.email != "") {
          if (data.password != "") {
            if (data.confirm_password != "") {
              updateViews("address");
            } else {
              console.log("Confirm password vacio");
            }
          } else {
            console.log("Password vacio");
          }
        } else {
          console.log("Correo vacio");
        }
      } else {
        console.log("Usuario vacio");
      }
    } else {
      console.log("esta vacio todos");
    }
  };

  // Change the state for show icon in input password
  const updateSecureTextEntry = () => {
    setData({
      ...data,
      secureTextEntry: !data.secureTextEntry,
    });
  };

  // Change the state for show icon in input confirm password
  const updateConfirmSecureTextEntry = () => {
    setData({
      ...data,
      confirm_secureTextEntry: !data.confirm_secureTextEntry,
    });
  };

  return (
    <View style={styles.container}>
      <StatusBar backgroundColor="#05375a" barStyle="light-content" />
      <View style={styles.header}>
        <Animatable.Text style={styles.text_header} animation="zoomInDown">
          ¡Registrate Ahora!
        </Animatable.Text>
      </View>

      <Animatable.View style={styles.footer} animation="fadeInUpBig">
        <Text
          style={[
            styles.text_footer,
            { color: "#039bd1", marginBottom: 5, fontWeight: "bold" },
          ]}
        >
          {view.showViewDataEntry
            ? "DATOS DE INGRESO"
            : view.showViewDataPersonal
            ? "DATOS PERSONALES"
            : view.showViewDataAddress
            ? "DATOS DE DOMICILIO"
            : ""}
        </Text>
        <ScrollView>
          {view.showViewDataEntry ? (
            <ScrollView>
              <Text style={styles.text_footer}>Nombre de Usuario</Text>
              <View style={styles.action}>
                <FontAwesome name="user-o" color="#05375a" size={20} />
                <TextInput
                  placeholder="Tu Usuario"
                  style={styles.textInput}
                  autoCapitalize="none"
                  autoCorrect={false}
                  value={data.user}
                  onChangeText={(value) => textInputUserChange(value)}
                />
                {data.check_textInputUserChange ? (
                  <Animatable.View animation="bounceIn">
                    <Feather name="check-circle" color="green" size={20} />
                  </Animatable.View>
                ) : null}
              </View>
              {data.isValidUser ? null : (
                <Animatable.View animation="fadeInLeft" duration={500}>
                  <Text style={styles.errorMsg}>
                    Debe tener mínimo 4 carácteres.
                  </Text>
                </Animatable.View>
              )}

              <Text style={[styles.text_footer, { marginTop: "8%" }]}>
                Correo
              </Text>
              <View style={styles.action}>
                <FontAwesome name="at" color="#05375a" size={20} />
                <TextInput
                  placeholder="Tu Correo"
                  style={styles.textInput}
                  autoCapitalize="none"
                  autoCompleteType="email"
                  autoCorrect={false}
                  keyboardType="email-address"
                  value={data.email}
                  onChangeText={(value) => textInputEmailChange(value)}
                />
                {data.check_textInputEmailChange ? (
                  <Animatable.View animation="bounceIn">
                    <Feather name="check-circle" color="green" size={20} />
                  </Animatable.View>
                ) : null}
              </View>
              {data.isValidEmail ? null : (
                <Animatable.View animation="fadeInLeft" duration={500}>
                  <Text style={styles.errorMsg}>Ingresa un correo válido.</Text>
                </Animatable.View>
              )}

              <Text style={[styles.text_footer, { marginTop: "8%" }]}>
                Contraseña
              </Text>
              <View style={styles.action}>
                <Feather name="lock" color="#05375a" size={20} />
                <TextInput
                  placeholder="Tu Contraseña"
                  secureTextEntry={data.secureTextEntry ? true : false}
                  style={styles.textInput}
                  autoCapitalize="none"
                  value={data.password}
                  onChangeText={(value) => handlePasswordChange(value)}
                />
                <TouchableOpacity onPress={updateSecureTextEntry}>
                  {data.secureTextEntry ? (
                    <Feather name="eye-off" color="grey" size={20} />
                  ) : (
                    <Feather name="eye" color="grey" size={20} />
                  )}
                </TouchableOpacity>
              </View>
              {data.isValidPassword ? null : (
                <Animatable.View animation="fadeInLeft" duration={500}>
                  <Text style={styles.errorMsg}>
                    Debe tener minimo 8 carácteres.
                  </Text>
                </Animatable.View>
              )}

              <Text style={[styles.text_footer, { marginTop: "8%" }]}>
                Confirmar Contraseña
              </Text>
              <View style={styles.action}>
                <Feather name="lock" color="#05375a" size={20} />
                <TextInput
                  placeholder="Confirma Tu Contraseña"
                  secureTextEntry={data.confirm_secureTextEntry ? true : false}
                  style={styles.textInput}
                  autoCapitalize="none"
                  value={data.confirm_password}
                  onChangeText={(value) => handleConfirmPasswordChange(value)}
                />
                <TouchableOpacity onPress={updateConfirmSecureTextEntry}>
                  {data.confirm_secureTextEntry ? (
                    <Feather name="eye-off" color="grey" size={20} />
                  ) : (
                    <Feather name="eye" color="grey" size={20} />
                  )}
                </TouchableOpacity>
              </View>
              {data.passwordCoincide ? null : (
                <Animatable.View animation="fadeInLeft" duration={500}>
                  <Text style={styles.errorMsg}>
                    Las contraseñas no coinciden.
                  </Text>
                </Animatable.View>
              )}
              <View style={[styles.button, { marginTop: 15 }]}>
                <LinearGradient
                  colors={["#08d4c4", "#039bd1"]}
                  style={styles.signIn}
                >
                  <TouchableOpacity
                    onPress={check_InputsEntry}
                    style={styles.signIn}
                  >
                    <Text style={[styles.textSign, { color: "#fff" }]}>
                      Siguiente
                    </Text>
                  </TouchableOpacity>
                </LinearGradient>

                <TouchableOpacity
                  onPress={() => navigation.goBack()}
                  style={[
                    styles.signIn,
                    { borderColor: "#039bd1", borderWidth: 1, marginTop: 15 },
                  ]}
                >
                  <Text style={[styles.textSign, { color: "#039bd1" }]}>
                    Iniciar Sesión
                  </Text>
                </TouchableOpacity>
              </View>
            </ScrollView>
          ) : view.showViewDataPersonal ? (
            <ScrollView>
              <Text style={styles.text_footer}>Nombre</Text>
              <View style={styles.action}>
                <FontAwesome name="user-o" color="#05375a" size={20} />
                <TextInput
                  placeholder="Tu Nombre"
                  style={styles.textInput}
                  autoCorrect={false}
                  autoCompleteType="name"
                  value={data.name}
                  onChangeText={(value) => textInputNameChange(value)}
                />
                {data.check_InputNameChange ? (
                  <Animatable.View animation="bounceIn">
                    <Feather name="check-circle" color="green" size={20} />
                  </Animatable.View>
                ) : null}
              </View>
              {data.isValidName ? null : (
                <Animatable.View animation="fadeInLeft" duration={500}>
                  <Text style={styles.errorMsg}>
                    Debe tener mínimo 3 carácteres.
                  </Text>
                </Animatable.View>
              )}

              <Text style={[styles.text_footer, { marginTop: "8%" }]}>
                Apellido Paterno
              </Text>
              <View style={styles.action}>
                <FontAwesome name="user-o" color="#05375a" size={20} />
                <TextInput
                  placeholder="Tu Apellido Paterno"
                  style={styles.textInput}
                  autoCorrect={false}
                  autoCompleteType="name"
                  value={data.lastname}
                  onChangeText={(value) => textInputAppChange(value)}
                />
                {data.check_InputAppChange ? (
                  <Animatable.View animation="bounceIn">
                    <Feather name="check-circle" color="green" size={20} />
                  </Animatable.View>
                ) : null}
              </View>
              {data.isValidApp ? null : (
                <Animatable.View animation="fadeInLeft" duration={500}>
                  <Text style={styles.errorMsg}>
                    Debe tener mínimo 3 carácteres.
                  </Text>
                </Animatable.View>
              )}

              <Text style={[styles.text_footer, { marginTop: "8%" }]}>
                Apellido Materno
              </Text>
              <View style={styles.action}>
                <FontAwesome name="user-o" color="#05375a" size={20} />
                <TextInput
                  placeholder="Tu Apellido Materno"
                  style={styles.textInput}
                  autoCorrect={false}
                  autoCompleteType="name"
                  value={data.lastnamematter}
                  onChangeText={(value) => textInputApmChange(value)}
                />
                {data.check_InputApmChange ? (
                  <Animatable.View animation="bounceIn">
                    <Feather name="check-circle" color="green" size={20} />
                  </Animatable.View>
                ) : null}
              </View>
              {data.isValidApm ? null : (
                <Animatable.View animation="fadeInLeft" duration={500}>
                  <Text style={styles.errorMsg}>
                    Debe tener mínimo 3 carácteres.
                  </Text>
                </Animatable.View>
              )}

              <Text style={[styles.text_footer, { marginTop: "8%" }]}>
                Edad
              </Text>
              <View style={styles.action}>
                <MaterialCommunityIcons
                  name="numeric"
                  color="#05375a"
                  size={20}
                />
                <TextInput
                  placeholder="Tu Edad"
                  style={styles.textInput}
                  autoCapitalize="none"
                  autoCorrect={false}
                  keyboardType="number-pad"
                  value={data.year}
                  maxLength={3}
                  onChangeText={(value) => textInputYearChange(value)}
                />
                {data.check_InputYearChange ? (
                  <Animatable.View animation="bounceIn">
                    <Feather name="check-circle" color="green" size={20} />
                  </Animatable.View>
                ) : null}
              </View>
              {data.isValidYear ? null : (
                <Animatable.View animation="fadeInLeft" duration={500}>
                  <Text style={styles.errorMsg}>
                    Debes ser mayor de edad y tener menos de 120.
                  </Text>
                </Animatable.View>
              )}

              <Text style={[styles.text_footer, { marginTop: "8%" }]}>
                Teléfono
              </Text>
              <View style={styles.action}>
                <Feather name="phone" color="#05375a" size={20} />
                <TextInput
                  placeholder="Tu Teléfono"
                  style={styles.textInput}
                  autoCapitalize="none"
                  autoCorrect={false}
                  autoCompleteType="tel"
                  keyboardType="number-pad"
                  value={data.telephone}
                  maxLength={10}
                  onChangeText={(value) => textInputPhoneChange(value)}
                />
                {data.check_InputPhoneChange ? (
                  <Animatable.View animation="bounceIn">
                    <Feather name="check-circle" color="green" size={20} />
                  </Animatable.View>
                ) : null}
              </View>
              {data.isValidPhone ? null : (
                <Animatable.View animation="fadeInLeft" duration={500}>
                  <Text style={styles.errorMsg}>Debe tener 10 números.</Text>
                </Animatable.View>
              )}
              <View style={[styles.button, { marginTop: 15 }]}>
                <View style={{ flexDirection: "row" }}>
                  <LinearGradient
                    colors={["#42DF90", "#149E8E"]}
                    style={[styles.signIn, { width: "49%" }]}
                  >
                    <TouchableOpacity
                      onPress={() => updateViews("entry")}
                      style={styles.signIn}
                    >
                      <Text style={[styles.textSign, { color: "#fff" }]}>
                        Regresar
                      </Text>
                    </TouchableOpacity>
                  </LinearGradient>
                  <LinearGradient
                    colors={["#08d4c4", "#039bd1"]}
                    style={[styles.signIn, { width: "49%", marginLeft: "2%" }]}
                  >
                    <TouchableOpacity
                      onPress={check_InputsPersonal}
                      style={styles.signIn}
                    >
                      <Text style={[styles.textSign, { color: "#fff" }]}>
                        Siguiente
                      </Text>
                    </TouchableOpacity>
                  </LinearGradient>
                </View>

                <TouchableOpacity
                  onPress={() => navigation.goBack()}
                  style={[
                    styles.signIn,
                    { borderColor: "#039bd1", borderWidth: 1, marginTop: 15 },
                  ]}
                >
                  <Text style={[styles.textSign, { color: "#039bd1" }]}>
                    Iniciar Sesión
                  </Text>
                </TouchableOpacity>
              </View>
            </ScrollView>
          ) : null}
        </ScrollView>
      </Animatable.View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#039bd1",
  },
  header: {
    flex: 1,
    justifyContent: "flex-end",
    paddingHorizontal: 20,
    paddingBottom: 50,
  },
  footer: {
    flex: 3,
    backgroundColor: "#fff",
    borderTopLeftRadius: 30,
    borderTopRightRadius: 30,
    paddingHorizontal: 20,
    paddingVertical: 30,
  },
  text_header: {
    color: "#fff",
    fontWeight: "bold",
    fontSize: 30,
  },
  text_footer: {
    color: "#05375a",
    fontSize: 18,
  },
  action: {
    flexDirection: "row",
    marginTop: 10,
    borderBottomWidth: 1,
    borderBottomColor: "#f2f2f2",
    paddingBottom: 5,
  },
  actionError: {
    flexDirection: "row",
    marginTop: 10,
    borderBottomWidth: 1,
    borderBottomColor: "#FF0000",
    paddingBottom: 5,
  },
  textInput: {
    flex: 1,
    marginTop: Platform.OS === "ios" ? 0 : -4,
    paddingLeft: 10,
    color: "#05375a",
  },
  errorMsg: {
    color: "#FF0000",
    fontSize: 14,
  },
  button: {
    alignItems: "center",
    marginTop: 50,
  },
  signIn: {
    width: "100%",
    height: 50,
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 10,
  },
  textSign: {
    fontSize: 18,
    fontWeight: "bold",
  },
});
