import React, { useState } from "react";
import {
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
  ScrollView,
  Image,
  AsyncStorage,
} from "react-native";
import Input from "../components/Input";
import PreLoader from "../components/PreLoader";
import Toast from "../utils/mensajeEnVentana";

export default function Login({ navigation }) {
  const [email, setEmail] = useState("");
  const [pass, setPass] = useState("");
  const [errorE, setErrorE] = useState("");
  const [errorP, setErrorP] = useState("");
  const [visible, setVisible] = useState(false);

  const emailRef = React.createRef();
  const passRef = React.createRef();

  function validarInputs() {
    if (email === "" && pass === "") {
      emailRef.current.shake();
      passRef.current.shake();
      emailRef.current.focus();
      setErrorE("EL CORREO ES NECESARIO");
      setErrorP("LA CONTRASEÑA ES NECESARIA");
    } else if (email === "") {
      emailRef.current.shake();
      emailRef.current.focus();
      setErrorE("EL CORREO ES NECESARIO");
    } else if (pass === "") {
      passRef.current.shake();
      passRef.current.focus();
      setErrorP("LA CONTRASEÑA ES NECESARIA");
    } else {
      validarUsuario();
      setVisible(true);
      emailRef.current.clear();
      passRef.current.clear();
    }
  }

  async function validarUsuario() {
    try {
      const url =
        "https://api-cochinito.herokuapp.com/login/" + email + "/" + pass;
      const response = await fetch(url);
      const json = await response.json();

      if (json != "No se encuentra una cuenta con este correo") {
        if (json != "Usuario y/o contraseña incorrecta") {
          const token = parseInt(json.id);
          console.log(token);
          await AsyncStorage.setItem("Token", "" + token);
          setEmail("");
          setPass("");
          setVisible(false);
          navigation.navigate("BotoneraScreen");
        } else {
          Toast("Correo y/o contraseña incorrecta");
          emailRef.current.shake();
          passRef.current.shake();
          setEmail("");
          setPass("");
        }
      } else {
        Toast("Correo y/o contraseña incorrecta");
        emailRef.current.shake();
        passRef.current.shake();
        setEmail("");
        setPass("");
      }
    } catch (error) {
      console.error(error);
    }
  }

  function mostrarLoader() {
    if (visible) {
      return <PreLoader isVisible={true} color={"#fff"} />;
    } else {
      return <Text style={styles.textbot}>Iniciar Sesion</Text>;
    }
  }

  return (
    <ScrollView
      style={{ height: "100%", width: "100%", backgroundColor: "#fff" }}
    >
      <View style={styles.container}>
        <View style={styles.titulo}>
          <Text style={styles.texttittle}>Cochinito</Text>
        </View>

        <Image
          source={require("../../assets/images/log.png")}
          style={styles.imagen}
        />

        <View style={styles.superior}>
          <Input
            ref={emailRef}
            placeholder="Correo"
            label="Ingresa tu correo"
            onChangeText={(value) => {
              setEmail(value);
              setErrorE("");
            }}
            keyboardType="email-address"
            autoCapitalize="none"
            autoCompleteType="email"
            errorMessage={errorE}
          />
          <Input
            ref={passRef}
            placeholder="Contraseña"
            label="Ingresa tu contraseña"
            onChangeText={(value) => {
              setPass(value);
              setErrorP("");
            }}
            secureTextEntry={true}
            autoCapitalize="none"
            errorMessage={errorP}
          />
        </View>
        <View style={styles.botones}>
          <TouchableOpacity
            style={styles.button}
            onPress={() => {
              validarInputs();
            }}
          >
            {mostrarLoader()}
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.button}
            onPress={() => {
              //alert("Estamos trabajando en eso :)");
              navigation.navigate("RegistroScreen");
            }}
          >
            <Text style={styles.textbot}>Registrarse</Text>
          </TouchableOpacity>
        </View>
      </View>
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  imagen: {
    width: 300,
    height: 120,
    marginTop: 15,
    resizeMode: "contain",
  },
  container: {
    flex: 1,
    backgroundColor: "#fff",
    flexDirection: "column",
    marginTop: 30,
    alignItems: "center",
    justifyContent: "flex-start",
  },
  titulo: {
    flex: 0.4,
    backgroundColor: "#fff",
    alignItems: "center",
  },
  superior: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    marginTop: 0,
  },
  botones: {
    flex: 1,
    backgroundColor: "#fff",
    alignContent: "center",
    alignItems: "center",
    marginTop: "0%",
    width: "100%",
  },
  title: {
    flex: 0.5,
    backgroundColor: "#fff",
    justifyContent: "center",
    alignItems: "center",
    marginTop: "0%",
  },
  listas: {
    flex: 3,
    backgroundColor: "#fff",
  },
  textOverlayTittle: {
    marginTop: 20,
    color: "#000",
    fontSize: 20,
    fontWeight: "bold",
  },
  textOverlayChild: {
    color: "#000",
    fontSize: 15,
  },
  text: {
    color: "#039be5",
    fontSize: 20,
    textAlign: "left",
    textTransform: "uppercase",
    fontWeight: "bold",
  },
  texttittle: {
    color: "#039be5",
    fontSize: 30,
    textAlign: "left",
    fontWeight: "bold",
  },
  textsaldo: {
    color: "#039be0",
    fontSize: 30,
    textAlign: "left",
  },
  button: {
    borderColor: "red",
    backgroundColor: "#039be5",
    paddingVertical: 10,
    borderRadius: 25,
    height: 45,
    width: "40%",
    marginLeft: "5%",
    marginRight: 10,
    marginTop: 30,
  },
  textbot: {
    color: "white",
    fontSize: 20,
    textAlign: "center",
    fontWeight: "bold",
  },
});
