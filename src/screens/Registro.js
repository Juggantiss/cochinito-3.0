import React, { useState } from "react";
import { StyleSheet, View, Text, ScrollView, Switch } from "react-native";
import Toast from "../utils/mensajeEnVentana";
import Input from "../components/Input";
import { Button, Overlay } from "react-native-elements";

export default function Registro({ navigation }) {
  //referencias
  const nom = React.createRef();
  const app = React.createRef();
  const apm = React.createRef();
  const tel = React.createRef();
  const ed = React.createRef();
  const cal = React.createRef();
  const ciu = React.createRef();
  const est = React.createRef();
  const cp = React.createRef();
  const co = React.createRef();
  const us = React.createRef();
  const pas = React.createRef();
  const pas2 = React.createRef();

  //variables
  const [nombre, setNombre] = useState("");
  const [apellidop, setApellidop] = useState("");
  const [appellidom, setApellidoM] = useState("");
  const [telefono, setTelefono] = useState("");
  const [edad, setEdad] = useState("");
  const [calle, setCalle] = useState("");
  const [ciudad, setCiudad] = useState("");
  const [estado, setEstado] = useState("");
  const [codigo, setCodigo] = useState("");
  const [correo, setCorreo] = useState("");
  const [usuario, setUsuario] = useState("");
  const [password, setPassword] = useState("");
  const [password2, setPassword2] = useState("");
  const [load, setLoad] = useState(false);
  const [isEnabled, setIsEnabled] = useState(false);
  const toggleSwitch = () => setIsEnabled((previousState) => !previousState);
  const [visible, setVisible] = useState(false);

  //errores
  const [errorN, setErrorN] = useState("");
  const [errorAp, setErrorAp] = useState("");
  const [errorAm, setErrorAm] = useState("");
  const [errorT, setErrorT] = useState("");
  const [errorE, setErrorE] = useState("");
  const [errorCal, setErrorCal] = useState("");
  const [errorCiu, setErrorCiu] = useState("");
  const [errorEst, setErrorEst] = useState("");
  const [errorCp, setErrorCp] = useState("");
  const [errorCor, setErrorCor] = useState("");
  const [errorU, setErrorU] = useState("");
  const [errorPas, setErrorPas] = useState("");
  const [errorPas2, setErrorPas2] = useState("");

  function validarCampos() {
    if (
      nombre === "" &&
      apellidop === "" &&
      appellidom === "" &&
      telefono === "" &&
      edad === "" &&
      calle === "" &&
      ciudad === "" &&
      estado === "" &&
      codigo === "" &&
      correo === "" &&
      usuario === "" &&
      password === "" &&
      password2 === ""
    ) {
      setErrorN("EL NOMBRE ES REQUERIDO");
      nom.current.shake();
      setErrorAp("EL APELLIDO PATERNO ES REQUERIDO");
      app.current.shake();
      setErrorAm("EL APELLIDO MATERNO ES REQUERIDO");
      apm.current.shake();
      setErrorT("EL NUMERO DE TELEFONO ES REQUERIDO");
      tel.current.shake();
      setErrorE("LA EDAD ES REQUERIDA");
      ed.current.shake();
      setErrorCal("LA CALLE ES REQUERIDA");
      cal.current.shake();
      setErrorCiu("LA CIUDAD ES REQUERIDA");
      ciu.current.shake();
      setErrorEst("EL ESTADO ES REQUERIDO");
      est.current.shake();
      setErrorCp("EL CODIGO POSTAL ES REQUERIDO");
      cp.current.shake();
      setErrorCor("EL CORREO ELECTRONICO ES REQUERIDO");
      co.current.shake();
      setErrorU("EL NOMBRE DE USUARIO ES REQUERIDO");
      us.current.shake();
      setErrorPas("LA CONTRASEÑA ES REQUERIDA");
      pas.current.shake();
      setErrorPas2("DEBES CONFIRMAR LA CONTRASEÑA");
      pas2.current.shake();
      console.log("estan vacios todos");
    } else {
      if (nombre === "") {
        setErrorN("EL NOMBRE ES REQUERIDO");
        nom.current.shake();
        Toast("EL NOMBRE ES REQUERIDO");
      } else if (apellidop === "") {
        setErrorAp("EL APELLIDO PATERNO ES REQUERIDO");
        app.current.shake();
        Toast("EL APELLIDO PATERNO ES REQUERIDO");
      } else if (appellidom === "") {
        setErrorAm("EL APELLIDO MATERNO ES REQUERIDO");
        apm.current.shake();
        Toast("EL APELLIDO MATERNO ES REQUERIDO");
      } else if (telefono === "") {
        setErrorT("EL NUMERO DE TELEFONO ES REQUERIDO");
        tel.current.shake();
        Toast("EL NUMERO DE TELEFONO ES REQUERIDO");
      } else if (telefono.length != 10) {
        setErrorT("EL NUMERO DE TELEFONO DEBE SER 10 DIGITOS");
        tel.current.shake();
        Toast("EL NUMERO DE TELEFONO DEBE SER 10 DIGITOS");
      } else if (edad === "") {
        setErrorE("LA EDAD ES REQUERIDA");
        ed.current.shake();
        Toast("LA EDAD ES REQUERIDA");
      } else {
        const edad_int = parseInt(edad);
        if (edad_int < 18) {
          setErrorE("DEBES SER MAYOR DE EDAD");
          ed.current.shake();
          Toast("DEBES SER MAYOR DE EDAD");
        } else if (calle === "") {
          setErrorCal("LA CALLE ES REQUERIDA");
          cal.current.shake();
          Toast("LA CALLE ES REQUERIDA");
        } else if (ciudad === "") {
          setErrorCiu("LA CIUDAD ES REQUERIDA");
          ciu.current.shake();
          Toast("LA CIUDAD ES REQUERIDA");
        } else if (estado === "") {
          setErrorEst("EL ESTADO ES REQUERIDO");
          est.current.shake();
          Toast("EL ESTADO ES REQUERIDO");
        } else if (codigo === "") {
          setErrorCp("EL CODIGO POSTAL ES REQUERIDO");
          cp.current.shake();
          Toast("EL CODIGO POSTAL ES REQUERIDO");
        } else if (correo === "") {
          setErrorCor("EL CORREO ELECTRONICO ES REQUERIDO");
          co.current.shake();
          Toast("EL CORREO ELECTRONICO ES REQUERIDO");
        } else if (usuario === "") {
          setErrorU("EL NOMBRE DE USUARIO ES REQUERIDO");
          us.current.shake();
          Toast("EL NOMBRE DE USUARIO ES REQUERIDO");
        } else if (password === "") {
          setErrorPas("LA CONTRASEÑA ES REQUERIDA");
          pas.current.shake();
          Toast("LA CONTRASEÑA ES REQUERIDA");
        } else if (password2 === "") {
          setErrorPas2("DEBES CONFIRMAR LA CONTRASEÑA");
          pas2.current.shake();
          Toast("DEBES CONFIRMAR LA CONTRASEÑA");
        } else if (password2 != password) {
          setErrorPas2("LAS CONTRASEÑAS NO COINCIDEN");
          setErrorPas("LAS CONTRASEÑAS NO COINCIDEN");
          pas2.current.shake();
          pas.current.shake();
          Toast("LAS CONTRASEÑAS NO COINCIDEN");
        } else if (!isEnabled) {
          Toast("Debes aceptar los terminos y condiciones");
        } else {
          setLoad(true);
          console.log("todo bien");
          registrarUsuario();
        }
      }
    }
  }

  async function registrarUsuario() {
    try {
      const url = "https://api-cochinito.herokuapp.com/usuario/create";
      const nueva_edad = parseInt(edad);
      const response = await fetch(url, {
        method: "POST",
        body: JSON.stringify({
          nombre,
          app: apellidop,
          apm: appellidom,
          telefono,
          edad: nueva_edad,
          calle,
          ciudad,
          estado,
          codigo_postal: codigo,
          email: correo,
          usuario,
          pass: password,
        }),
        headers: {
          "Content-Type": "application/json",
        },
      });
      const json = await response.json();
      if (json.resultado == "Usuario agregado") {
        console.log(json.resultado);
        Toast(json.resultado);
        limpiarCampos();
        setLoad(false);
        navigation.navigate("LoginScreen");
      } else {
        if (json.resultado == "Debes ser mayor de edad") {
          setErrorE("DEBES SER MAYOR DE EDAD");
          setLoad(false);
        } else if (json.resultado == "Este correo ya se encuentra registrado") {
          setErrorCor("ESTE CORREO YA SE ENCUENTRA REGISTRADO");
          setLoad(false);
        } else if (
          json.resultado == "Este usuario ya se encuentra registrado"
        ) {
          setErrorU("ESTE USUARIO YA SE ENCUENTRA REGISTRADO");
          setLoad(false);
        } else {
          console.log(json.resultado);
          Toast(json.resultado);
          limpiarCampos();
          setLoad(false);
        }
      }
    } catch (error) {
      console.log(error);
      Toast("ERROR AL REGISTRAR EL USUARIO");
      console.log("ERROR AL REGISTRAR EL USUARIO");
      limpiarCampos();
      setLoad(false);
    }
  }

  async function limpiarCampos() {
    setNombre("");
    setApellidop("");
    setApellidoM("");
    setTelefono("");
    setEdad("");
    setCalle("");
    setCiudad("");
    setEstado("");
    setCorreo("");
    setCodigo("");
    setUsuario("");
    setPassword("");
    setPassword2("");
  }

  return (
    <ScrollView>
      <View style={styles.container}>
        <Overlay
          isVisible={visible}
          height={"40%"}
          onBackdropPress={() => {
            setVisible(false);
          }}
        >
          <ScrollView>
            <Text style={{ fontSize: 15, textAlign: "justify" }}>
              Al aceptar los términos y condiciones estás de acuerdo con:
            </Text>
            <Text style={{ fontSize: 15, textAlign: "justify" }}>
              1. Todos los datos proporcionados en esta aplicación serán
              unicamente para uso exclusivo dentro de la aplicación.
            </Text>
            <Text style={{ fontSize: 15, textAlign: "justify" }}>
              2. En caso de ser requerido aclaraciones por parte del
              administrador de las cuentas compartidas, se podrá proporcionar
              alguno de los datos necesarios.
            </Text>
            <Text style={{ fontSize: 15, textAlign: "justify" }}>
              3. En las cuentas compartidas el administrador(creador) de la
              cuenta, será el único que podrá tener acceso a algunos datos del
              usuario.
            </Text>
            <Text style={{ fontSize: 15, textAlign: "justify" }}>
              4. Todos los datos solicitados será utilizado para tener mayor
              seguridad al compartir cuentas, y poder llevar un control de
              estos.
            </Text>
          </ScrollView>
        </Overlay>
        <View style={styles.titulo}>
          <Text style={styles.titulo}>Registro</Text>
        </View>
        <View style={styles.inputs}>
          <Input
            ref={nom}
            placeholder="Nombre"
            label="Ingresa el nombre"
            onChangeText={(value) => {
              setNombre(value);
              setErrorN("");
            }}
            errorMessage={errorN}
            value={nombre}
          />
          <Input
            ref={app}
            placeholder="Apellido Paterno"
            label="Ingresa el apellido paterno"
            onChangeText={(value) => {
              setApellidop(value);
              setErrorAp("");
            }}
            errorMessage={errorAp}
            value={apellidop}
          />
          <Input
            ref={apm}
            placeholder="Apellido Materno"
            label="Ingresa el apellido materno"
            onChangeText={(value) => {
              setApellidoM(value);
              setErrorAm("");
            }}
            errorMessage={errorAm}
            value={appellidom}
          />
          <Input
            ref={tel}
            placeholder="Telefono"
            inputContainerStyle={styles.contorno}
            label="Ingresa el telefono"
            onChangeText={(value) => {
              setTelefono(value);
              setErrorT("");
            }}
            keyboardType="numeric"
            errorMessage={errorT}
            value={telefono}
          />
          <Input
            ref={ed}
            placeholder="Edad"
            label="Ingresa la edad"
            inputContainerStyle={styles.contorno2}
            onChangeText={(value) => {
              setEdad(value);
              setErrorE("");
            }}
            keyboardType="numeric"
            errorMessage={errorE}
            value={edad}
          />
          <Input
            ref={cal}
            placeholder="Calle"
            label="Ingresa la calle"
            onChangeText={(value) => {
              setCalle(value);
              setErrorCal("");
            }}
            errorMessage={errorCal}
            value={calle}
          />
          <Input
            ref={ciu}
            placeholder="Ciudad"
            label="Ingresa la ciudad"
            onChangeText={(value) => {
              setCiudad(value);
              setErrorCiu("");
            }}
            errorMessage={errorCiu}
            value={ciudad}
          />
          <Input
            ref={est}
            placeholder="Estado"
            label="Ingresa el estado"
            onChangeText={(value) => {
              setEstado(value);
              setErrorEst("");
            }}
            errorMessage={errorEst}
            value={estado}
          />
          <Input
            ref={cp}
            placeholder="Codigo Postal"
            inputContainerStyle={styles.contorno}
            label="Ingresa el codigo postal"
            onChangeText={(value) => {
              setCodigo(value);
              setErrorCp("");
            }}
            keyboardType="numeric"
            errorMessage={errorCp}
            value={codigo}
          />
          <Input
            ref={co}
            placeholder="Correo Electronico"
            label="Ingresa el correo electronico"
            onChangeText={(value) => {
              setCorreo(value);
              setErrorCor("");
            }}
            keyboardType="email-address"
            errorMessage={errorCor}
            value={correo}
          />
          <Input
            ref={us}
            placeholder="Usuario"
            label="Ingresa el nombre de usuario"
            onChangeText={(value) => {
              setUsuario(value);
              setErrorU("");
            }}
            errorMessage={errorU}
            value={usuario}
          />
          <Input
            ref={pas}
            placeholder="Contraseña"
            label="Ingresa la contraseña"
            onChangeText={(value) => {
              setPassword(value);
              setErrorPas("");
            }}
            secureTextEntry={true}
            errorMessage={errorPas}
            value={password}
          />
          <Input
            ref={pas2}
            placeholder="Confirma contraseña"
            label="Ingresa de nuevo la contraseña"
            onChangeText={(value) => {
              setPassword2(value);
              setErrorPas2("");
            }}
            secureTextEntry={true}
            errorMessage={errorPas2}
            value={password2}
          />
        </View>
        <View style={styles.swit}>
          <Switch
            trackColor={{ false: "#767577", true: "#000" }}
            thumbColor={isEnabled ? "#039be5" : "#f4f3f4"}
            ios_backgroundColor="#3e3e3e"
            onValueChange={toggleSwitch}
            value={isEnabled}
          />
          <Text style={{ fontSize: 15 }}>Aceptar</Text>
          <Text
            style={{ fontSize: 15, color: "#039be5" }}
            onPress={() => {
              setVisible(true);
            }}
          >
            {" "}
            términos y condiciones
          </Text>
        </View>
        <View style={styles.inferior}>
          <Button
            title="Registrarse"
            buttonStyle={styles.boton}
            titleStyle={styles.textBoton}
            loading={load}
            loadingStyle={{ paddingRight: "15%" }}
            onPress={() => validarCampos()}
          />
        </View>
      </View>
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    flexDirection: "column",
    marginTop: 25,
    justifyContent: "flex-start",
  },
  titulo: {
    flex: 0.5,
    backgroundColor: "#fff",
    alignItems: "center",
    color: "#039be5",
    fontSize: 30,
    textAlign: "left",
    textTransform: "uppercase",
    fontWeight: "bold",
  },
  text: {
    color: "#039be5",
    fontSize: 20,
    textAlign: "left",
    textTransform: "uppercase",
    fontWeight: "bold",
  },
  inputs: {
    flex: 4,
    backgroundColor: "#fff",
    alignItems: "flex-start",
    justifyContent: "flex-start",
    marginTop: 0,
  },
  inferior: {
    flex: 0.5,
    marginTop: 15,
    marginBottom: 10,
    backgroundColor: "#fff",
    alignItems: "center",
    flexDirection: "column",
    justifyContent: "flex-start",
  },
  boton: {
    paddingRight: "18%",
    width: "80%",
    height: 45,
    marginTop: 15,
    backgroundColor: "#039be5",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 4,
    },
    shadowOpacity: 0.32,
    shadowRadius: 5.46,
    elevation: 9,
  },
  textBoton: {
    textAlign: "center",
    textTransform: "uppercase",
    fontWeight: "bold",
    fontSize: 20,
  },
  contorno: {
    width: "40%",
    borderWidth: 2,
    borderColor: "transparent",
    borderBottomColor: "#039be5",
  },
  contorno2: {
    width: "30%",
    borderWidth: 2,
    borderColor: "transparent",
    borderBottomColor: "#039be5",
  },
  swit: {
    flex: 0.5,
    marginTop: 20,
    backgroundColor: "#fff",
    alignItems: "center",
    flexDirection: "row",
    justifyContent: "center",
  },
});
