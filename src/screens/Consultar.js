import React, { useState, useEffect } from "react";
import {
  StyleSheet,
  View,
  Text,
  Picker,
  TouchableOpacity,
  ScrollView,
  AsyncStorage,
} from "react-native";
import { ListItem, Overlay } from "react-native-elements";
import PreLoader from "../components/PreLoader";

import Toast from "../utils/mensajeEnVentana";

export default function Consultar({ navigation }) {
  const [selectedValue, setSelectedValue] = useState("Selecciona una cuenta");
  const [OverlayVis, setOverVisible] = useState(false);
  const [movVisible, setMovVisible] = useState(false);
  const [VisibleSaldo, setVisibleSaldo] = useState(true);
  const [saldo, setSaldo] = useState(0);
  const [tipo, setTipo] = useState(0);
  const [depo, setDepo] = useState([]);
  const [ret, setRet] = useState([]);
  const [id, setId] = useState(0);
  const [cuenta, setCuenta] = useState([]);
  const [datos, setDatos] = useState({});

  function traerDepositos() {
    const url =
      "https://api-cochinito.herokuapp.com/movimiento/deposito/" +
      selectedValue;
    fetch(url)
      .then((response) => response.json())
      .then((movi) => {
        const mov = movi.Deposito;
        mov.reverse();
        setDepo(mov);
      })
      .catch((err) => {
        console.log(err);
        Toast("ERROR AL TRAER LOS DEPOSITOS");
      })
      .finally(() => {
        setMovVisible(false);
      });
  }

  function traerRetiros() {
    const url =
      "https://api-cochinito.herokuapp.com/movimiento/retiro/" + selectedValue;
    fetch(url)
      .then((response) => response.json())
      .then((movi) => {
        const mov = movi.Retiro;
        mov.reverse();
        setRet(mov);
      })
      .catch((err) => {
        console.log(err);
        Toast("ERROR AL TRAER LOS RETIROS");
      })
      .finally(() => setMovVisible(false));
  }

  async function traerCuentasUser() {
    const getToken = async () => {
      const token = await AsyncStorage.getItem("Token");
      if (token) {
        setId(token);
      }
      console.log("token " + token);
      const url =
        "https://api-cochinito.herokuapp.com/rtiene/one/cuenta/" + token;
      try {
        const response = await fetch(url);
        const data = await response.json();
        setCuenta(data.Cuenta);
      } catch (error) {
        console.log(error);
        Toast("ERROR AL TRAER LAS CUENTAS");
      }
    };
    getToken();
  }

  useEffect(() => {
    traerCuentasUser();
  }, []);

  useEffect(() => {
    const unsubscribe = navigation.addListener("focus", async () => {
      traerCuentasUser();
      setDepo([]);
      setRet([]);
      setTipo(3);
    });
    return unsubscribe;
  }, [navigation]);

  function mostrarCuentas() {
    return cuenta.map((item, i) => (
      <Picker.Item
        label={item.descripcion}
        value={item.clv_cuenta}
        key={item.clv_cuenta}
      />
    ));
  }

  function mostrarMovimiento() {
    if (tipo == 1) {
      return (
        <ScrollView>
          {depo.map((item, i) => (
            <ListItem
              key={i}
              title={item.usuario.usuario}
              subtitle={item.fecha_movimiento + " | " + item.hora}
              rightTitle={item.cantidad + ""}
              bottomDivider
              chevron
              onPress={() => {
                setDatos(item);
                setOverVisible(true);
              }}
            />
          ))}
        </ScrollView>
      );
    } else {
      return (
        <ScrollView>
          {ret.map((item, i) => (
            <ListItem
              key={i}
              title={item.usuario.usuario}
              subtitle={item.fecha_movimiento + " | " + item.hora}
              rightTitle={item.cantidad + ""}
              bottomDivider
              chevron
              onPress={() => {
                setDatos(item);
                setOverVisible(true);
              }}
            />
          ))}
        </ScrollView>
      );
    }
  }

  function vistaMovimiento() {
    if (movVisible) {
      return <PreLoader isVisible={movVisible} color={"#039be5"} />;
    } else {
      switch (tipo) {
        case 1:
          return <Text style={styles.texttittle}>Lista de Depositos</Text>;
          break;
        case 2:
          return <Text style={styles.texttittle}>Lista de Retiros</Text>;
          break;

        default:
          return <Text style={styles.texttittle}></Text>;
          break;
      }
    }
  }

  function vistaOverlay() {
    switch (tipo) {
      case 1:
        return (
          <View>
            <Text style={styles.text}>Datos del deposito</Text>
            <Text style={styles.textOverlayTittle}>Cantidad:</Text>
            <Text style={styles.textOverlayChild}>{datos.cantidad}</Text>
            <Text style={styles.textOverlayTittle}>Saldo anterior:</Text>
            <Text style={styles.textOverlayChild}>{datos.saldo_anterior}</Text>
            <Text style={styles.textOverlayTittle}>Fecha:</Text>
            <Text style={styles.textOverlayChild}>
              {datos.fecha_movimiento}
            </Text>
            <Text style={styles.textOverlayTittle}>Hora:</Text>
            <Text style={styles.textOverlayChild}>{datos.hora}</Text>
          </View>
        );
        break;
      case 2:
        return (
          <ScrollView>
            <Text style={styles.text}>Datos del retiro</Text>
            <Text style={styles.textOverlayTittle}>Cantidad:</Text>
            <Text style={styles.textOverlayChild}>{datos.cantidad}</Text>
            <Text style={styles.textOverlayTittle}>Saldo anterior:</Text>
            <Text style={styles.textOverlayChild}>{datos.saldo_anterior}</Text>
            <Text style={styles.textOverlayTittle}>Fecha:</Text>
            <Text style={styles.textOverlayChild}>
              {datos.fecha_movimiento}
            </Text>
            <Text style={styles.textOverlayTittle}>Hora:</Text>
            <Text style={styles.textOverlayChild}>{datos.hora}</Text>
            <Text style={styles.textOverlayTittle}>Motivo:</Text>
            <Text style={styles.textOverlayChild}>{datos.motivo}</Text>
          </ScrollView>
        );
        break;
      default:
        return (
          <View>
            <Text></Text>
          </View>
        );
        break;
    }
  }

  let lblSaldo = "";
  function mostrarSaldo() {
    const url =
      "https://api-cochinito.herokuapp.com/cuenta/one/" + selectedValue;
    fetch(url)
      .then((response) => response.json())
      .then((json) => setSaldo(json.saldo))
      .catch((err) => console.log(err))
      .finally(() => setVisibleSaldo(false));
    lblSaldo = "$ " + saldo;

    if (VisibleSaldo) {
      return <PreLoader isVisible={VisibleSaldo} color={"#039be5"} />;
    } else {
      return <Text style={styles.textsaldo}>{lblSaldo}</Text>;
    }
  }

  return (
    <View style={styles.container}>
      <Overlay
        isVisible={OverlayVis}
        onBackdropPress={() => {
          setOverVisible(false);
        }}
      >
        {vistaOverlay()}
      </Overlay>

      <View style={styles.titulo}>
        <Text style={styles.texttittle}>Saldo actual</Text>
      </View>

      <View style={styles.superior}>
        {mostrarSaldo()}
        <Picker
          selectedValue={selectedValue}
          style={{ height: 50, width: 210, marginTop: 10 }}
          onValueChange={(itemValue, itemIndex) => {
            setSelectedValue(itemValue);
            setDepo([]);
            setRet([]);
            setTipo(3);
          }}
        >
          {mostrarCuentas()}
        </Picker>
      </View>

      <View style={styles.botones}>
        <TouchableOpacity
          style={styles.button}
          onPress={() => {
            setMovVisible(true);
            traerDepositos();
            setTipo(1);
          }}
        >
          <Text style={styles.textbot}>Depositos</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.button}
          onPress={() => {
            setMovVisible(true);
            traerRetiros();
            setTipo(2);
          }}
        >
          <Text style={styles.textbot}>Retiros</Text>
        </TouchableOpacity>
      </View>

      <View style={styles.title}>{vistaMovimiento()}</View>

      <View style={styles.listas}>{mostrarMovimiento()}</View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    flexDirection: "column",
    marginTop: 25,
    justifyContent: "flex-start",
  },
  titulo: {
    flex: 0.5,
    backgroundColor: "#fff",
    alignItems: "center",
  },
  superior: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    marginTop: 0,
  },
  botones: {
    flex: 0.5,
    backgroundColor: "#fff",
    flexDirection: "row",
    justifyContent: "flex-start",
    marginTop: "0%",
  },
  title: {
    flex: 0.5,
    backgroundColor: "#fff",
    justifyContent: "center",
    alignItems: "center",
    marginTop: "0%",
  },
  listas: {
    flex: 3,
    backgroundColor: "#fff",
  },
  textOverlayTittle: {
    marginTop: 20,
    color: "#000",
    fontSize: 20,
    fontWeight: "bold",
  },
  textOverlayChild: {
    color: "#000",
    fontSize: 15,
  },
  text: {
    color: "#039be5",
    fontSize: 20,
    textAlign: "left",
    textTransform: "uppercase",
    fontWeight: "bold",
  },
  texttittle: {
    color: "#039be5",
    fontSize: 30,
    textAlign: "left",
    fontWeight: "bold",
  },
  textsaldo: {
    color: "#039be0",
    fontSize: 30,
    textAlign: "left",
  },
  button: {
    borderColor: "red",
    backgroundColor: "#039be5",
    paddingVertical: 10,
    borderRadius: 25,
    height: 45,
    width: "40%",
    marginLeft: "5%",
    marginRight: 10,
  },
  textbot: {
    color: "white",
    fontSize: 20,
    textAlign: "center",
    fontWeight: "bold",
  },
});
