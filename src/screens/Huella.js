import React, { useState, useRef, useEffect } from "react";
import {
  View,
  Text,
  StyleSheet,
  Animated,
  Image,
  TouchableOpacity,
  AsyncStorage,
} from "react-native";
import * as huella from "expo-local-authentication";
import { Ionicons } from "@expo/vector-icons";

export default function Huella({ navigation }) {
  const [compatible, setCompatible] = useState(false);
  const [guardado, setGuardado] = useState(false);
  const [error, setError] = useState("Toca el sensor para continuar");
  const [errores, setErrores] = useState(0);
  //const [intentos, setIntentos] = useState(0);
  const [cancel, setCancel] = useState(false);
  const fadeAnim = useRef(new Animated.Value(0)).current;
  let intentos = 0;

  function fadeIn() {
    Animated.timing(fadeAnim, {
      toValue: 1,
      duration: 100,
    }).start();
  }

  function fadeOut() {
    Animated.timing(fadeAnim, {
      toValue: 0,
      duration: 200,
    }).start();
  }

  async function buscarToken() {
    const getToken = async () => {
      try {
        const token = await AsyncStorage.getItem("Token");
        if (token) {
          console.log("si hay");
          verificarHuella();
        } else {
          console.log("no hay");
          navigation.navigate("LoginScreen");
        }
        console.log("token " + token);
      } catch (error) {
        console.log(error);
      }
    };
    getToken();
  }

  async function verificarHuella() {
    const comp = await huella.hasHardwareAsync();
    setCompatible(comp);
    console.log("es compatible: " + comp);
    const guard = await huella.isEnrolledAsync();
    setGuardado(guard);
    console.log("tiene guardado: " + guard);
    if (comp) {
      if (guard) {
        console.log("si hay guardados");
        fadeIn();
        escanear();
      } else {
        console.log("no hay guardados");
        navigation.navigate("LoginScreen");
      }
    } else {
      console.log("no es compatible");
      navigation.navigate("LoginScreen");
    }
  }

  async function escanear() {
    console.log("----------------------------------------");
    console.log("Inicia el escaneo");
    console.log("escanear " + intentos);
    huella.cancelAuthenticate();
    const result = await huella.authenticateAsync();
    if (result.success) {
      setError("Toca el sensor para continuar");
      fadeOut();
      navigation.navigate("BotoneraScreen");
    } else {
      setError("No coincide con la huella guardada");
      intentos++;
      verificarErrores();
    }
  }

  function verificarErrores() {
    console.log("errores " + intentos);
    if (intentos <= 5) {
      verificarHuella();
    } else {
      setError("Has excedido el número de intentos");
      huella.cancelAuthenticate();
      intentos = 0;
    }
  }

  useEffect(() => {
    buscarToken();
  }, []);

  useEffect(() => {
    const unsubscribe = navigation.addListener("focus", async () => {
      buscarToken();
    });
    return unsubscribe;
  }, [navigation]);

  function mostrarVista() {
    if (cancel) {
      return (
        <Animated.View style={styles.header}>
          <Image
            source={require("../../assets/images/huella.png")}
            style={{
              ...styles.imagen,
              backgroundColor: cancel ? "#fff" : "#BDBDBD",
              marginTop: cancel ? 0 : "25%",
            }}
          />
          <Text
            style={{
              fontSize: 16,
              marginTop: 15,
              textAlign: "center",
              marginLeft: "20%",
              marginRight: "20%",
              marginBottom: "30%",
            }}
          >
            Valida tu identidad con el método de desbloqueo de tu celular para
            ingresar a la app
          </Text>
          <TouchableOpacity
            style={styles.button}
            onPress={() => {
              setCancel(false);
              fadeIn();
              setError("Toca el sensor para continuar");
              verificarHuella();
            }}
          >
            <Text style={styles.textbot}>Validar identidad</Text>
          </TouchableOpacity>
          <Text style={{ fontSize: 16, marginTop: 15 }}>
            O si lo prefieres
            <Text
              style={{ color: "#039be5", fontWeight: "bold" }}
              onPress={() => {
                navigation.navigate("LoginScreen");
              }}
            >
              {" "}
              inicia sesión
            </Text>
          </Text>
        </Animated.View>
      );
    } else {
      return (
        <>
          <Animated.View style={styles.header}>
            <Image
              source={require("../../assets/images/huella.png")}
              style={{
                ...styles.imagen,
                backgroundColor: cancel ? "#fff" : "#BDBDBD",
                marginTop: cancel ? "50%" : "25%",
              }}
            />
          </Animated.View>

          <Animated.View style={[styles.footer, { opacity: fadeAnim }]}>
            <Text style={styles.tittle}>Confirma con tu huella</Text>
            <Ionicons name="md-finger-print" size={64} color="#039be5" />
            <Text style={{ color: "#FF5252", marginTop: 15, fontSize: 15 }}>
              {error}
            </Text>
            <Text
              style={styles.cancelar}
              onPress={() => {
                setCancel(true);
                fadeOut();
              }}
            >
              Cancelar
            </Text>
          </Animated.View>
        </>
      );
    }
  }

  return (
    <View
      style={{
        ...styles.container,
        backgroundColor: cancel ? "#fff" : "#BDBDBD",
      }}
    >
      {mostrarVista()}
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  header: {
    flex: 60,
    justifyContent: "center",
    alignItems: "center",
    alignContent: "center",
  },
  footer: {
    flex: 40,
    backgroundColor: "#fff",
    borderTopLeftRadius: 15,
    borderTopRightRadius: 15,
    justifyContent: "center",
    alignItems: "center",
    alignContent: "center",
  },
  tittle: {
    position: "absolute",
    left: 20,
    top: 20,
    fontSize: 16,
  },
  cancelar: {
    color: "#039be5",
    position: "absolute",
    fontSize: 14,
    left: 18,
    bottom: 18,
    textTransform: "uppercase",
    fontWeight: "bold",
  },
  imagen: {
    width: "80%",
    height: 300,
    resizeMode: "contain",
  },
  button: {
    borderColor: "red",
    backgroundColor: "#039be5",
    borderRadius: 5,
    height: 45,
    width: "70%",
    justifyContent: "center",
  },
  textbot: {
    color: "white",
    fontSize: 16,
    textAlign: "center",
    fontWeight: "bold",
  },
});
