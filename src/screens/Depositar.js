import React, { useState, useEffect } from "react";
import {
  StyleSheet,
  View,
  Text,
  Picker,
  ScrollView,
  AsyncStorage,
  TouchableOpacity,
} from "react-native";
import Input from "../components/Input";
import Toast from "../utils/mensajeEnVentana";
import PreLoader from "../components/PreLoader";

export default function Depositar({ navigation }) {
  const [selectedValue, setSelectedValue] = useState("Selecciona una cuenta");
  const [cuenta, setCuenta] = useState([]);
  const [cantidad, setCantidad] = useState("");
  const [id, setId] = useState(0);
  const [error, setError] = useState("");
  const input = React.createRef();
  const [loader, setLoader] = useState(false);

  function validarCantidad() {
    if (cantidad === "" || cantidad === "0") {
      setError("DEBES INGRESAR UNA CANTIDAD");
      input.current.shake();
      input.current.focus();
      console.log("error");
    } else {
      DepositarSaldo();
      setLoader(true);
      setCantidad("");
      input.current.clear();
    }
  }

  async function DepositarSaldo() {
    try {
      const cant = parseFloat(cantidad);
      const url =
        "https://api-cochinito.herokuapp.com/movimiento/create/" +
        id +
        "/1/" +
        selectedValue;
      const response = await fetch(url, {
        method: "POST",
        body: JSON.stringify({
          cantidad: cant,
          motivo: "",
        }),
        headers: {
          "Content-Type": "application/json",
        },
      });
      const json = await response.json();
      if (json.resultado != "Deposito agregado") {
        Toast("ERROR AL HACER EL DEPOSITO");
        input.current.shake();
        input.current.clear();
      } else {
        console.log(json.resultado);
        Toast(json.resultado);
      }
      console.log(json.resultado);
      setLoader(false);
    } catch (error) {
      console.log(error);
      Toast("ERROR AL HACER EL DEPOSITO");
      input.current.shake();
      input.current.clear();
      setLoader(false);
    }
  }

  async function traerCuentasUser() {
    const getToken = async () => {
      const token = await AsyncStorage.getItem("Token");
      if (token) {
        setId(token);
      }
      console.log("token " + token);
      try {
        const url =
          "https://api-cochinito.herokuapp.com/rtiene/one/cuenta/" + token;
        const response = await fetch(url);
        const data = await response.json();
        setCuenta(data.Cuenta);
      } catch (error) {
        console.log(error);
        Toast("ERROR AL TRAER LAS CUENTAS");
      }
    };
    getToken();
  }

  useEffect(() => {
    traerCuentasUser();
  }, []);

  useEffect(() => {
    const unsubscribe = navigation.addListener("focus", async () => {
      traerCuentasUser();
    });
    return unsubscribe;
  }, [navigation]);

  function mostrarCuenta() {
    return cuenta.map((item, i) => (
      <Picker.Item
        label={item.descripcion}
        value={item.clv_cuenta}
        key={item.clv_cuenta}
      />
    ));
  }

  function mostrarLoader() {
    if (loader) {
      return <PreLoader isVisible={true} color={"#fff"} />;
    } else {
      return <Text style={styles.textbot}>Depositar</Text>;
    }
  }

  return (
    <ScrollView style={{ backgroundColor: "#fff" }}>
      <View style={styles.container}>
        <View style={styles.titulo}>
          <Text style={styles.text}>Deposito</Text>
        </View>
        <View style={styles.superior}>
          <Text style={styles.text}>Cuenta</Text>
          <Picker
            selectedValue={selectedValue}
            style={{ height: 50, width: 210, marginTop: 30, marginBottom: 20 }}
            onValueChange={(itemValue, itemIndex) =>
              setSelectedValue(itemValue)
            }
          >
            {mostrarCuenta()}
          </Picker>
          <Input
            ref={input}
            inputContainerStyle={styles.contorno}
            placeholder="Cantidad"
            label="Ingresa la cantidad"
            onChangeText={(value) => {
              setCantidad(value);
              setError("");
            }}
            keyboardType="decimal-pad"
            errorMessage={error}
          />
        </View>
        <View style={styles.botones}>
          <TouchableOpacity
            style={styles.button}
            onPress={() => {
              validarCantidad();
            }}
          >
            {mostrarLoader()}
          </TouchableOpacity>
        </View>
      </View>
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  container: {
    width: "100%",
    height: "100%",
    backgroundColor: "#fff",
    flexDirection: "column",
    marginTop: 30,
    justifyContent: "flex-start",
  },
  botones: {
    flex: 3,
    backgroundColor: "#fff",
    alignItems: "center",
    flexDirection: "column",
    justifyContent: "flex-start",
    marginTop: 80,
    marginBottom: 40,
  },
  titulo: {
    flex: 0.5,
    backgroundColor: "#fff",
    marginLeft: 10,
  },
  text: {
    color: "#039be5",
    fontSize: 20,
    textAlign: "left",
    textTransform: "uppercase",
    fontWeight: "bold",
  },
  superior: {
    flex: 2,
    backgroundColor: "#fff",
    alignItems: "center",
    marginTop: 40,
  },
  button: {
    borderColor: "red",
    backgroundColor: "#039be5",
    paddingVertical: 10,
    borderRadius: 25,
    height: 45,
    width: "50%",
    marginTop: 15,
  },
  textbot: {
    color: "white",
    fontSize: 20,
    textAlign: "center",
    fontWeight: "bold",
  },
  contorno: {
    width: "40%",
    borderWidth: 2,
    borderColor: "transparent",
    borderBottomColor: "#039be5",
  },
});
