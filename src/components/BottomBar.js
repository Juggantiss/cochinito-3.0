import React from "react";
import { createMaterialBottomTabNavigator } from "@react-navigation/material-bottom-tabs";
import DepositarScreen from "../screens/Depositar";
import RetirarScreen from "../screens/Retirar";
import MainScreen from "../screens/Main";
import ConsultarScreen from "../screens/Consultar";
import HomeCuentaScreen from "../screens/cuenta/HomeCuenta";
import AboutScreen from "../screens/About";
import {
  MaterialIcons,
  Entypo,
  MaterialCommunityIcons,
} from "@expo/vector-icons";

const Tab = createMaterialBottomTabNavigator();

export default function Botonera() {
  return (
    <Tab.Navigator
      initialRouteName="Consultar"
      activeColor="#f0edf6"
      inactiveColor="#039be5"
      barStyle={{ backgroundColor: "#039be5" }}
    >
      <Tab.Screen
        name="Consultar"
        component={ConsultarScreen}
        options={{
          tabBarLabel: "Consultar",
          tabBarIcon: ({ color }) => (
            <MaterialIcons name="account-balance" size={25} color="white" />
          ),
        }}
      />
      <Tab.Screen
        name="Depositar"
        component={DepositarScreen}
        options={{
          tabBarLabel: "Depositar",
          tabBarIcon: ({ color }) => (
            <MaterialIcons name="create" size={25} color="white" />
          ),
        }}
      />
      <Tab.Screen
        name="Retirar"
        component={RetirarScreen}
        options={{
          tabBarLabel: "Retirar",
          tabBarIcon: ({ color }) => (
            <Entypo name="retweet" size={25} color="white" />
          ),
        }}
      />
      <Tab.Screen
        name="Cuentas"
        component={HomeCuentaScreen}
        options={{
          tabBarLabel: "Cuentas",
          tabBarIcon: ({ color }) => (
            <MaterialCommunityIcons
              name="account-group"
              size={25}
              color="white"
            />
          ),
        }}
      />
      <Tab.Screen
        name="Acerca de"
        component={AboutScreen}
        options={{
          tabBarLabel: "Acerca de",
          tabBarIcon: ({ color }) => (
            <Entypo name="list" size={25} color="white" />
          ),
        }}
      />
    </Tab.Navigator>
  );
}
