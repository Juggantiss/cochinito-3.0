import React, { forwardRef } from "react";
import Icon from "react-native-vector-icons/FontAwesome";
import { Input } from "react-native-elements";

import { styles } from "./styles";

export default forwardRef(({ referencia, iconName, ...props }, ref) => (
  <Input
    ref={ref}
    inputContainerStyle={{
      ...styles.inputContainerStyle,
    }}
    containerStyle={{
      ...styles.containerStyle,
    }}
    inputStyle={{
      ...styles.inputStyle,
    }}
    labelStyle={{
      ...styles.labelStyle,
    }}
    leftIcon={<Icon name={iconName} size={24} color="black" />}
    errorStyle={{ ...styles.error }}
    {...props}
  />
));
