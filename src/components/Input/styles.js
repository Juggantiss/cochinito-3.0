import { StyleSheet } from "react-native";

const COLOR_BASE = "#039be5";
const MARGEN_TOP = 20;

export const styles = {
  inputContainerStyle: {
    width: "65%",
    borderWidth: 2,
    borderColor: "transparent",
    borderBottomColor: COLOR_BASE,
  },
  containerStyle: {
    alignItems: "center",
    alignContent: "center",
    justifyContent: "center",
    marginTop: MARGEN_TOP,
  },
  inputStyle: {
    fontSize: 15,
    marginLeft: 10,
    color: COLOR_BASE,
  },
  labelStyle: {
    color: COLOR_BASE,
    marginTop: MARGEN_TOP,
  },
  error: {
    color: "red",
  },
};
