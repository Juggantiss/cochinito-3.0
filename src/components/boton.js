import React from "react";
import { Text, TouchableOpacity, StyleSheet } from "react-native";
import PreLoader from "../components/PreLoader";

const styles = StyleSheet.create({
  button: {
    borderColor: "red",
    paddingVertical: 10,
    borderRadius: 25,
  },
  text: {
    color: "white",
    fontSize: 20,
    textAlign: "center",
    fontWeight: "bold",
  },
});

const Boton = ({
  titulo,
  alPresionar,
  margenSuperior,
  tipo,
  ancho,
  color,
  visible,
}) => {
  return (
    <TouchableOpacity
      style={[
        styles.button,
        margenSuperior ? { marginTop: margenSuperior } : { marginTop: 0 },
        ancho ? { width: ancho } : { width: "60%" },
        color ? { backgroundColor: color } : { backgroundColor: "#039be5" },
      ]}
      onPress={alPresionar}
    >
      <Text
        style={[
          styles.text,
          tipo ? { textTransform: tipo } : { textTransform: null },
        ]}
      >
        {titulo}
      </Text>
    </TouchableOpacity>
  );
};

export default Boton;
